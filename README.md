# Quickom Conference Sdk For Android Tv Example

  This Android project showcases the steps to install and use quickom conference sdk library for android tv.
  Latest version: 1.0.3

------------
## Requirement
- minsdkversion: 21
- kotlin version: 1.6.10 above

## Installation
### Step 1: Update build.gradle inside the application module
- Add the following code to **build.gradle** (or **settings.gradle**) inside the application module that will be using the library published on GitHub Packages Repository
```gradle
    repositories {
        maven {
            name = "GitHubPackages"
            url = uri("https://maven.pkg.github.com/quickomofficial/quickom_conference_sdk_android_tv")

            credentials {
                username = "quickomofficial"
                password = "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"
            }
        }
        maven { url "https://jitpack.io" }
    }
```

### Step 2: Enable data binding and add library
- Enable data binding in build.gradle of app module
```gradle
    plugins {
        ...
        id 'kotlin-kapt'
    }
```
```gradle
android {
    ...
    buildFeatures {
        dataBinding true
    }
}
```

- Inside dependencies of the build.gradle of app module, use the following code
```gradle
    dependencies {
        implementation 'com.quickom:conferencesdk:${latest_version}'
        ...
    }
```

## Setup
- In your application file add the following code in function onCreate()
```kotlin
    class YourApplication : Application() {
        override fun onCreate() {
            super.onCreate()

            // Init call center sdk
            QuickomConferenceSdk.init(this, "Your language (only two letter with lowercase)")
        }
    }
```
* Note: Call Center Sdk right now only support for two languages: english (en) and vietnamese (vi). Default language: vietnamese

- If you enable proguard for your project then add those lines in your app's proguard file
```text
-dontoptimize
-dontshrink

-keep class kotlin.** { *; }
-keep class kotlinx.** { *; }
-keep class kotlin.Metadata { *; }
-dontwarn kotlin.**
-dontwarn kotlinx.**
-keepclassmembers class **$WhenMappings {
    <fields>;
}
-keepclassmembers class kotlin.Metadata {
    public <methods>;
}
-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
}

-dontwarn androidx.databinding.**
-keep class androidx.databinding.** { *; }
-keep class * extends androidx.databinding.DataBinderMapper

-keep class org.webrtc.** { *; }

-keepclassmembers class com.quickom.conferencesdk.tv.domain.model.* { *; }
```

## Events
### QuickomConferenceSdkListener
- This interface is a callback of all events return to your app from quickom conference sdk every time you call function of quickom conference sdk
```kotlin
    val listener = object : QuickomConferenceSdk.QuickomConferenceSdkListener {
       override fun onSuccess() {
            // return success for your function call
       }

       override fun onError(errorCode: Int, errorDescription: String) {
            // return error for your function call
       }
   }
```

- ErrorCode

    | Code                  | Description                                                 |
    |-----------------------|-------------------------------------------------------------|
    | 10100                 | Code is invalid                                             |
    | 10101                 | Passcode is wrong                                           |
    | -1                    | Error unknown                                               |


## Usage
### Join a conference
- Add the following code every time you want to join a conference
```kotlin
    /**
    * Function to join a conference
    * @param code: code to join conference
    * @param passcode: passcode of conference (if conference was set)
    * @param name: your name use to join conference (default name: Participant)
    */
    fun joinRoom(code: String?, passcode: String?, name: String?, listener: QuickomConferenceSdkListener) {
        QuickomConferenceSdk.INSTANCE.joinRoom(code, passcode, name, listener)
    }
```

### Start a conference
- Add the following code every time you want to start a conference from existing code
```kotlin
    /**
    * Function to start a conference from existing code
    * @param token: token of account quickom
    * @param code: code to start conference from account quickom
    * @param name: your name use to start conference (default name: Host)
    */
    fun startRoom(token: String?, code: String?, name: String?, listener: QuickomConferenceSdkListener) {
        QuickomConferenceSdk.INSTANCE.startRoom(token, code, name, listener)
    }
```

### Quick start a conference
- Add the following code every time you want to quick start a conference
```kotlin
    /**
    * Function to start a conference from existing code
    * @param token: token of account quickom
    * @param roomName: name of conference
    * @param name: your name use to quick start conference (default name: Host)
    */
    fun startRoomQuick(token: String?, roomName: String?, name: String?, listener: QuickomConferenceSdkListener) {
        QuickomConferenceSdk.INSTANCE.startRoomQuick(token, roomName, name, listener)
    }
```