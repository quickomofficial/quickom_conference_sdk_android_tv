# quickom_conference_sdk_android_tv changelog

## 1.0.1 (20/12/2021)
- Fix some minor bugs
- Add feature pin view
- Display name badge, logo and text

## 1.0.2 (7/3/2022)
- Fix some minor bugs
- Improve performance

## 1.0.3 (21/3/2022)
- Update README.md
- Fix some minor bugs
- Improve performance
- Add function start room