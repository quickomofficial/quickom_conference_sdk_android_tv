package com.conferencesdk.example

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.FragmentActivity
import com.quickom.conferencesdk.tv.QuickomConferenceSdk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import timber.log.Timber

@ExperimentalCoroutinesApi
class MainActivity : FragmentActivity() {
    private var loadingDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val etConferenceCode = findViewById<EditText>(R.id.et_conference_code)
        val etConferencePasscode = findViewById<EditText>(R.id.et_conference_passcode)
        val btJoinConference = findViewById<Button>(R.id.bt_join_conference)

        etConferenceCode.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action === KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
                    etConferencePasscode.requestFocus()
                    return true
                }
                return false
            }
        })

        etConferencePasscode.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent): Boolean {
                if (event.action === KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_ENTER) {
                    hideSoftKeyboard(etConferencePasscode)
                    btJoinConference.requestFocus()
                    return true
                }
                return false
            }
        })

        btJoinConference.setOnClickListener {
            showLoading()
            QuickomConferenceSdk.INSTANCE.joinRoom(etConferenceCode.text.toString(), etConferencePasscode.text.toString(), "Name", this, "",
                object : QuickomConferenceSdk.QuickomConferenceSdkListener {
                    override fun onSuccess() {
                        hideLoading()
                    }

                    override fun onError(errorCode: Int, errorDescription: String) {
                        hideLoading()
                        runOnUiThread {
                            showError(errorDescription)
                        }
                    }
                })
        }
    }

    fun showLoading() {
        loadingDialog = AlertDialog.Builder(this, R.style.Theme_AppCompat_Dialog_Alert)
            .setTitle("Loading")
            .create()
        loadingDialog?.show()
    }

    fun hideLoading() {
        loadingDialog?.dismiss()
    }

    fun showError(error: String) {
        val alertDialog = AlertDialog.Builder(this, R.style.Theme_AppCompat_Dialog_Alert)
            .setTitle("Message")
            .setMessage(error)
            .setPositiveButton(android.R.string.ok, null)
            .create()
        alertDialog.show()
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).requestFocus()
    }

    fun hideSoftKeyboard(view: View) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }
}